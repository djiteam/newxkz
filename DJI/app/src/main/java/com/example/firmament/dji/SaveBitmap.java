package com.example.firmament.dji;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Firmament on 15/1/1.
 */

public class SaveBitmap {

        public static Integer photoNumber = 0;

        public static String photopath;

        public static void createSDCardDir(){
            File sdcardDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Images");
            photopath = sdcardDir.getPath() + "/";

            Log.e("aaaaa", photopath);

            if (!sdcardDir.mkdirs()) {
                Log.e("aaaaa", "dir already exist");
            } else {
                Log.e("aaaaa", "make new dir");
            }
        }

        public static void save(Bitmap bitmap) throws IOException{

            File picturefile = new File(photopath, photoNumber.toString() + ".jpg");

            if (picturefile.exists()){
                picturefile.delete();
            }

            FileOutputStream fileOutputStream = new FileOutputStream(picturefile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        }    }

