package com.example.firmament.dji;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;

import dji.sdk.api.DJIDrone;
import dji.sdk.api.GroundStation.DJIGroundStationTypeDef;
import dji.sdk.interfaces.DJIGroundStationExecutCallBack;

/**
 * Created by Firmament on 14-11-14.
 */

public class Control extends Fragment {

    public View layout;

    private Animation show;
    private Animation dis;

    private ImageButton one_setUp;
    private ImageButton one_setDown;

    private RelativeLayout left;
    private RelativeLayout right;

    private LeftControl_View leftControl_view;
    private RightControl_View rightControl_view;

    private float LeftSetX;
    private float LeftSetY;
    private float RightSetX;
    private float RightSetY;

    private int pitch = 0;
    private int roll = 0;
    private int yaw = 0;
    private int throttle = 0;
    private int down_cnt = 0;
    private Thread DownThread;
    private boolean downflag = true;
    private long startTime;
    private final int SHOWTOAST = 1;
    private float alt_ground=0;
    private float alt_now=0;
    private Animation animation;

    private ControlClickListener listener;

    private TextView textView1;
    private TextView textView2;


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.control,container,false);

        textView1 = (TextView)layout.findViewById(R.id.textView1);
        textView2 = (TextView)layout.findViewById(R.id.textView2);

        left = (RelativeLayout)layout.findViewById(R.id.left_control_layout);
        right = (RelativeLayout)layout.findViewById(R.id.right_control_layout);

        one_setUp = (ImageButton)layout.findViewById(R.id.one_setUp);
        one_setDown = (ImageButton)layout.findViewById(R.id.one_setDown);

        leftControl_view = new LeftControl_View(getActivity());
        rightControl_view = new RightControl_View(getActivity());

        left.addView(leftControl_view);
        right.addView(rightControl_view);


        //左摇杆控制；
        leftControl_view.setOnTouchListener(new View.OnTouchListener() {

            float maxR = DestinyUtil.dip2px(getActivity(),100);

            float startX = DestinyUtil.dip2px(getActivity(),100);
            float startY = DestinyUtil.dip2px(getActivity(),100);
            float resetX = leftControl_view.bitmap.getWidth()/2;
            float resetY = leftControl_view.bitmap.getHeight()/2;

            float X;
            float Y;

            float R;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        X = event.getX();
                        Y = event.getY();
                        leftControl_view.bitmapX = startX - resetX;
                        leftControl_view.bitmapY = startY - resetY;
                        leftControl_view.invalidate();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        X = event.getX();
                        Y = event.getY();
                        R = getDiagram(X,Y);

                        if (R <= maxR) {
                            leftControl_view.bitmapX = X - resetX;
                            leftControl_view.bitmapY = Y - resetY;
                            setLeft(X,Y);
                            leftControl_view.invalidate();

                            SetLeftJoystick(LeftSetX,LeftSetY);   //向飞机传输摇杆参数；
                            //textView1.setText(LeftSetX + ":" +LeftSetY);
                        }else {
                            leftControl_view.bitmapX = startX - resetX;
                            leftControl_view.bitmapY = startY - resetY;
                            setLeft(startX,startY);
                            leftControl_view.invalidate();

                            SetLeftJoystick(LeftSetX,LeftSetY);
                            //textView1.setText(LeftSetX + ":" +LeftSetY);
                        }

                        break;
                    case MotionEvent.ACTION_UP:
                        leftControl_view.bitmapX = startX - resetX;
                        leftControl_view.bitmapY = startY - resetY;
                        setLeft(startX,startY);
                        leftControl_view.invalidate();

                        SetLeftJoystick(LeftSetX,LeftSetY);
                        //textView1.setText(LeftSetX + ":" +LeftSetY);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        //右摇杆控制；
        rightControl_view.setOnTouchListener(new View.OnTouchListener() {

            float maxR = DestinyUtil.dip2px(getActivity(),100);

            float startX = DestinyUtil.dip2px(getActivity(),100);
            float startY = DestinyUtil.dip2px(getActivity(),100);
            float resetX = rightControl_view.bitmap.getWidth()/2;
            float resetY = rightControl_view.bitmap.getHeight()/2;

            float X;
            float Y;

            float R;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        X = event.getX();
                        Y = event.getY();
                        rightControl_view.bitmapX = startX - resetX;
                        rightControl_view.bitmapY = startY - resetY;
                        rightControl_view.invalidate();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        X = event.getX();
                        Y = event.getY();
                        R = getDiagram(X,Y);
                        if (R <= maxR) {
                            rightControl_view.bitmapX = X - resetX;
                            rightControl_view.bitmapY = Y - resetY;
                            setRight(X,Y);
                            rightControl_view.invalidate();

                            SetRightJoystick(RightSetX,RightSetY);  //向飞机传输摇杆参数；
                            //textView2.setText(RightSetX + ":" + RightSetY);
                        }else {
                            rightControl_view.bitmapX = startX - resetX;
                            rightControl_view.bitmapY = startY - resetY;
                            setRight(startX,startY);
                            rightControl_view.invalidate();

                            SetRightJoystick(RightSetX,RightSetY);
                            //textView2.setText(RightSetX + ":" + RightSetY);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        rightControl_view.bitmapX = startX - resetX;
                        rightControl_view.bitmapY = startY - resetY;
                        setRight(startX,startY);
                        rightControl_view.invalidate();

                        SetRightJoystick(RightSetX,RightSetY);
                        //textView2.setText(RightSetX + ":" + RightSetY);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        //一键起飞按钮
        one_setUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.oneKeyfly();
            }
        });

        //一键降落按钮
        one_setDown.setOnTouchListener(new View.OnTouchListener() {

            boolean check = false;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                downflag=true;
                DownThread = new Thread(){
                    public void run()
                    {
                        startTime=System.currentTimeMillis();   //获取开始时间
                        while (downflag) {
                            down_cnt++;
                            if (down_cnt == 4) {   //下降
                                down_cnt = 0;
                                Log.e("down cnt1", down_cnt + "");
                                DJIDrone.getDjiGroundStation().setAircraftJoystick(0, 0, 0, 2, new DJIGroundStationExecutCallBack() {
                                    @Override
                                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                        // TODO Auto-generated method stub
                                    }

                                });
                                try {
                                    Thread.currentThread().sleep(50);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else {     //悬停，降低下降速率
                                Log.e("down cnt2", down_cnt + "");
                                DJIDrone.getDjiGroundStation().setAircraftJoystick(0, 0, 0, 0, new DJIGroundStationExecutCallBack() {
                                    @Override
                                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                        // TODO Auto-generated method stub
                                    }

                                });
                                long endTime=System.currentTimeMillis(); //获取结束时间
                                if(endTime-startTime>=20000) {    //copter down end after 10s
                                    downflag = false;
                                    Log.e("stop!!!!!!!!!!!!!!!!!!!!!", down_cnt + "");
                                    new Thread() {
                                        public void run() {    //停止油门

                                            DJIDrone.getDjiGroundStation().setAircraftThrottle(0, new DJIGroundStationExecutCallBack() {

                                                @Override
                                                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                                    // TODO Auto-generated method stub

                                                }

                                            });
                                        }
                                    }.start();
                                    break;
                                }
                            }
                        }
                    }
                };

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        DownThread.start();
                        break;
                    case MotionEvent.ACTION_UP:
                        //Log.i("down cnt233333333333333333333333333333333333", down_cnt + "");
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        return layout;
    }

    @Override
    public void onDestroy(){
        dis = AnimationUtils.loadAnimation(getActivity(),R.anim.control_dis);
        //layout.setAnimation(dis);
        layout.startAnimation(dis);
        super.onDestroy();
    }

    /*
        on attach check attachment to its super activity
     */

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            listener = (ControlClickListener)activity;
        }catch (Exception e){
            Log.e("Control","can not find attached activity");
        }
    }

    private class LeftControl_View extends View {

        public Bitmap bitmap;

        public float bitmapX;
        public float bitmapY;

        public LeftControl_View(Context context){
            super(context);

            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.control_bar);

            bitmapX = DestinyUtil.dip2px(getActivity(),100) - bitmap.getWidth()/2;
            bitmapY = DestinyUtil.dip2px(getActivity(),100) - bitmap.getHeight()/2;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawBitmap(bitmap, bitmapX, bitmapY, null);

            if (bitmap.isRecycled()){
                bitmap.recycle();
            }
        }
    }

    private class RightControl_View extends View {

        public Bitmap bitmap;

        public float bitmapX;
        public float bitmapY;

        public RightControl_View(Context context){
            super(context);

            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.control_bar);

            bitmapX = DestinyUtil.dip2px(getActivity(),100) - bitmap.getWidth()/2;
            bitmapY = DestinyUtil.dip2px(getActivity(),100) - bitmap.getHeight()/2;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawBitmap(bitmap, bitmapX, bitmapY, null);

            if (bitmap.isRecycled()){
                bitmap.recycle();
            }
        }
    }

    private float getDiagram(float x,float y){
        float p = DestinyUtil.dip2px(getActivity(),100);
        float R = (float) Math.sqrt((Math.pow(x - p, 2) + Math.pow(y - p, 2)));

        return R;
    }

    private void setLeft(float X,float Y){
        float standardX = DestinyUtil.dip2px(getActivity(),100);
        float standardY = DestinyUtil.dip2px(getActivity(),100);

        LeftSetX = ((standardX - X)/standardX);
        LeftSetY = ((standardY - Y)/standardY);
    }

    private void setRight(float X,float Y){
        float standardX = DestinyUtil.dip2px(getActivity(),100);
        float standardY = DestinyUtil.dip2px(getActivity(),100);

        RightSetX = ((standardX - X)/standardX);
        RightSetY = ((standardY - Y)/standardY);
    }

    /*
         JoyStick add by lizi
         function : control flight Throttol and Yaw
         parameter: X: from -1 to +1; Y: from -1 to +1;
    */
    private void SetRightJoystick(float X,float Y){
        int PitchJoyControlMaxSpeed = 1000;
        int RollJoyControlMaxSpeed = 1000;

        pitch = (int)(-PitchJoyControlMaxSpeed * Y);
        roll = (int)(RollJoyControlMaxSpeed * X);

        Thread rightThread = new Thread() {
            public void run() {
                Log.e("Right", pitch + ";" + roll);
                DJIDrone.getDjiGroundStation().setAircraftJoystick(0, pitch, roll, 0, new DJIGroundStationExecutCallBack() {
                    @Override
                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                        // TODO Auto-generated method stub

                    }
                });
            }
        };
        rightThread.start();
    }

    /*
         JoyStick add by lizi
         function : control flight Pitch and Roll
         parameter: X: from -1 to +1; Y: from -1 to +1;
    */
    private void SetLeftJoystick(float X,float Y){

        int YawJoyControlMaxSpeed = 500;
        yaw = (int)(YawJoyControlMaxSpeed * X);
        throttle = 0;

        if(Y > 0.5){
            throttle = 1;
        }
        else if(Y <= -0.5){
            //if(pY == -1){
            //	throttle = 2;
            //}
            throttle = 2;
        }

        Thread leftThread = new Thread(){

            public void run(){
                Log.e("Left",yaw + ";" + throttle);
                DJIDrone.getDjiGroundStation().setAircraftJoystick(yaw, 0, 0, throttle,new DJIGroundStationExecutCallBack(){
                    @Override
                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        };
        leftThread.start();
    }
     /*
         JoyStick add by lizi end
    */


    /*
        Interface of Control by JCGuo
     */
    public interface ControlClickListener {
        public void oneKeyfly();
    }

    private class DROWN_Thread extends Thread{

        volatile boolean stop = false;


    }
}
