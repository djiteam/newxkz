package com.example.firmament.dji;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.util.HashMap;


public class Alberm extends Activity{

    private Gallery gallery;
    private ImageSwitcher imageSwitcher;
    private Animation animation1;
    private Animation animation2;

    private static int ifGalleryShowed = 0;

    private int image_position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alberm);
        animation1 = AnimationUtils.loadAnimation(this,R.anim.gallery_show);
        animation2 = AnimationUtils.loadAnimation(this,R.anim.gallery_dis);

        final ImageAdapter imageAdapter = new ImageAdapter(this);

        gallery = (Gallery)findViewById(R.id.my_gallery);
        gallery.setAdapter(imageAdapter);
        gallery.setVisibility(View.INVISIBLE);
        gallery.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                resetTime();
                return false;
            }
        });
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                image_position = position;
                imageSwitcher.setImageResource(imageAdapter.imageInteger[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ;
            }
        });

        imageSwitcher = (ImageSwitcher)findViewById(R.id.my_image);
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView = new ImageView(Alberm.this);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return imageView;
            }
        });
        imageSwitcher.setImageResource(imageAdapter.imageInteger[0]);
        imageSwitcher.setOnTouchListener(new View.OnTouchListener() {

            float X;
            float Y;
            float startX;
            float startY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        resetTime();
                        startX = event.getX();
                        startY = event.getY();
                        if (ifGalleryShowed == 0) {
                            gallery.startAnimation(animation1);
                            gallery.setVisibility(View.VISIBLE);
                            ifGalleryShowed = 1;
                            Message msg = mHandler.obtainMessage(ifGalleryShowed);
                            mHandler.sendMessageDelayed(msg, 3000);
                        } else {
                            ;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        resetTime();
                        break;
                    case MotionEvent.ACTION_UP:
                        X = event.getX();
                        Y = event.getY();
                        if (X - startX < -80) {
                            if (image_position < imageAdapter.imageInteger.length - 1) {
                                image_position++;
                                imageSwitcher.setInAnimation(Alberm.this, R.anim.photo_rightin);
                                imageSwitcher.setOutAnimation(Alberm.this, R.anim.photo_leftout);
                                gallery.setSelection(image_position);
                            }
                        } else if (X - startX > 80) {
                            if (image_position > 0) {
                                image_position--;
                                imageSwitcher.setInAnimation(Alberm.this, R.anim.photo_leftin);
                                imageSwitcher.setOutAnimation(Alberm.this, R.anim.photo_rightout);
                                gallery.setSelection(image_position);
                            }
                        } else {
                            ;
                        }
                        resetTime();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            if(msg.what == 1)
            {
                gallery.startAnimation(animation2);
                gallery.setVisibility(View.INVISIBLE);
                ifGalleryShowed = 0;
            }
        }
    };

    private void resetTime() {
        mHandler.removeMessages(ifGalleryShowed);
        Message msg = mHandler.obtainMessage(ifGalleryShowed);
        mHandler.sendMessageDelayed(msg, 3000);
    }

    private class ImageAdapter extends BaseAdapter {

        private Context context;

        private HashMap<Integer, View> ViewMaps;
        private int mCount;

        private LayoutInflater inflate;

        private ImageView imageView;

        //图片源数组
        private Integer[] imageInteger={
                R.drawable.ic_launcher,
                R.drawable.camara,
                R.drawable.campus,
                R.drawable.control,
                R.drawable.alberm,
                R.drawable.koda,
                R.drawable.video_begined,
                R.drawable.video_koda
        };

        public ImageAdapter(Context c){
            context = c;
            inflate = LayoutInflater.from(c);
            ViewMaps = new HashMap<Integer, View>(imageInteger.length);
            mCount = imageInteger.length;
        }

        @Override
        public int getCount() {
            return imageInteger.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = ViewMaps.get(position);

            if(view == null) {
                view = inflate.inflate(R.layout.gallery_items,null);
                imageView = (ImageView)view.findViewById(R.id.gallery_image);
                ViewMaps.put(position,view);
                imageView.setImageResource(imageInteger[position]);
            }
            return view;
        }
    }
}
