package com.example.firmament.dji;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by jcguo on 14/12/12.
 */


public class XKZFaceDetector {
    private static final String TAG = "XKZFaceDetector";
    private CascadeClassifier faceClassifier;
    private String faceCascadeName = "haarcascade_frontalface_alt2.xml";
    private String path = "/data/data/com.example.firmament.dji/database/";

    public XKZFaceDetector(Context context) {
        try {
            System.loadLibrary("opencv_java");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i(TAG,"in detector init");
        try {
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdir();
            }

            if (!(new File(path + faceCascadeName)).exists()){
                Log.i(TAG, "classifier file not found. Now copy it.");

                InputStream is = context.getResources().openRawResource(R.raw.haarcascade_frontalface_alt2);
                FileOutputStream fos = new FileOutputStream(path + faceCascadeName);
                byte[] buffer = new byte[4096];
                int count = 0;
                while ((count = is.read(buffer)) > 0){
                    fos.write(buffer, 0, count);
                }
                fos.close();
                is.close();
            } else {
                Log.e(TAG, "classifier file already exist.");
            }

            faceClassifier = new CascadeClassifier(path + faceCascadeName);

            if (faceClassifier != null) {
                Log.i(TAG,"load classifier successfully");
            } else {
                Log.i(TAG,"load classifier failed!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap detectFaces(Bitmap bitmap) {
        if (faceClassifier == null) return bitmap;

        Mat frame = new Mat();
        Mat frameGray = new Mat();
        MatOfRect faces = new MatOfRect();
        int num = 0;

        Utils.bitmapToMat(bitmap, frame);

        Imgproc.cvtColor(frame, frameGray, Imgproc.COLOR_BGR2GRAY);

        faceClassifier.detectMultiScale(frameGray, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                adjustedSize(frame.size(), 15), adjustedSize(frame.size(), 5));
        for (Rect rect : faces.toArray()) {
            num += 1;
            Core.rectangle(frame, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 255, 0));

            Log.i(TAG,"detected " + num + " face at " + rect.x + " " + rect.y + " " + (rect.x + rect.width) + " " + (rect.y + rect.height));
        }

        Utils.matToBitmap(frame, bitmap);
        return bitmap;
    }

    public Rect detectFaceRect(Bitmap bitmap) {
        Mat frame = new Mat();
        Mat frameGray = new Mat();
        MatOfRect faces = new MatOfRect();
        Rect[] faceArr;

        Utils.bitmapToMat(bitmap, frame);

        Imgproc.cvtColor(frame, frameGray, Imgproc.COLOR_BGR2GRAY);

        faceClassifier.detectMultiScale(frameGray, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                adjustedSize(frame.size(), 12), adjustedSize(frame.size(), 5));
        //Log.i(TAG,frame.width() + " * " + frame.height());

        faceArr = faces.toArray();
        if (faceArr.length > 0) {
            return faceArr[0];
        } else {
            return null;
        }
    }

    private Size adjustedSize(Size origin, double scale) {
        Size newSize = new Size((int)(origin.width/scale),(int)(origin.height/scale));
        return newSize;
    }

}
