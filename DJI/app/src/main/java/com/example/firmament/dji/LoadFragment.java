package com.example.firmament.dji;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

/**
 * Created by Firmament on 14/12/11.
 */

public class LoadFragment extends Fragment {

    //public Thread loadThread;

    private ImageView load_image;

    private View layout;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.loadfragment,container,false);
        load_image = (ImageView)layout.findViewById(R.id.load_image);

        Animation animation1 = new AlphaAnimation(1,0);
        animation1.setInterpolator(new LinearInterpolator());
        animation1.setDuration(800);
        animation1.setRepeatCount(Animation.INFINITE);
        animation1.setRepeatMode(Animation.REVERSE);
        load_image.setAnimation(animation1);

        return layout;
    }
}
