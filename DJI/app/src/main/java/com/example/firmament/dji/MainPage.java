package com.example.firmament.dji;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import dji.sdk.api.Battery.DJIBatteryProperty;
import dji.sdk.api.Camera.DJICameraSettingsTypeDef;
import dji.sdk.api.DJIDrone;
import dji.sdk.api.DJIDroneTypeDef;
import dji.sdk.api.DJIError;
import dji.sdk.api.GroundStation.DJIGroundStationFlyingInfo;
import dji.sdk.api.GroundStation.DJIGroundStationTask;
import dji.sdk.api.GroundStation.DJIGroundStationTypeDef;
import dji.sdk.api.MainController.DJIMainControllerSystemState;
import dji.sdk.interfaces.DJIBattryUpdateInfoCallBack;
import dji.sdk.interfaces.DJIExecuteResultCallback;
import dji.sdk.interfaces.DJIGerneralListener;
import dji.sdk.interfaces.DJIGroundStationExecutCallBack;
import dji.sdk.interfaces.DJIGroundStationFlyingInfoCallBack;
import dji.sdk.interfaces.DJIGroundStationHoverCallBack;
import dji.sdk.interfaces.DJIGroundStationOneKeyFlyCallBack;
import dji.sdk.interfaces.DJIMcuUpdateStateCallBack;
import dji.sdk.interfaces.DJIReceivedVideoDataCallBack;
import dji.sdk.widget.DjiGLSurfaceView;

public class MainPage extends Activity implements AdapterView.OnItemClickListener,
        Control.ControlClickListener, XKZGLSurfaceView.FaceDetectionListener {

    private int Mode_Type = 0;  //0正常模式，1遥控模式，2摄像模式；
    private boolean isControlShowed = false;    //ture表示遥控杆存在，false表示遥控杆不存在；
    private boolean Mode = true;     //true表示拍照，false表示摄像；
    public boolean isRecording = false;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private Animation show;
    private Animation dis;

    private Control control;
    private RelativeLayout control_toucher;
    private LoadFragment loadFragment;

    private ImageButton start_control;
    private ImageButton alberm;
    private ImageButton koda;
    private ToggleButton mode_switcher;
    private TextView textView;

    private XKZGLSurfaceView djiGLSurfaceView;
    private DJIReceivedVideoDataCallBack receivedVideoDataCallBack;
    //图像显示相关：

    //设置菜单栏；
    private int[] resArray = new int[]{R.drawable.ic_launcher, R.drawable.ic_launcher, R.drawable.ic_launcher, R.drawable.ic_launcher};
    private String[] title = new String[]{"1", "2", "3", "4"};
    private PopupWindow pw = null;
    private LayoutInflater inflater = null;

    //add by lizi
    private static final String TAG = "main page";
    private DJIMcuUpdateStateCallBack mMcuUpdateStateCallBack = null;
    private DJIGroundStationFlyingInfoCallBack mGroundStationFlyingInfoCallBack = null;
    private DJIBattryUpdateInfoCallBack mBattryUpdateInfoCallBack = null;
    private Timer mCheckModeForPauseTimer;
    private DJIGroundStationTypeDef.GroundStationControlMode mControlMode;
    private boolean getHomePiontFlag = false;
    private DJIGroundStationTask mTask;
    private double homeLocationLatitude = -1;
    private double homeLocationLongitude = -1;
    private double aircraftLocationLatitude = -1;
    private double aircraftLocationLongitude = -1;
    private boolean isFlying = false;
    private Timer mTimer;
    private Timer bTimer;

    private int pitch = 0;
    private int roll = 0;
    private int yaw = 0;
    private int throttle = 0;
    private int zoom_cnt = 0;
    private Thread zoomThread;
    private boolean zoomflag = true;
    private long startTime;
    private float X_target = 0;
    private float Y_target = 0;
    private int controlup_cnt = 0;
    private int controldown_cnt = 0;
    private int zoomup_cnt = 0;
    private int zoomdown_cnt = 0;
    private boolean stopflag = true;
    double Pitch_now = 180;
    double Roll_now = 180;
    private int power = 100;
    StringBuffer sb;
    private final int SHOWTOAST = 1;

    class Task extends TimerTask {
        //int times = 1;
        @Override
        public void run()
        {
            //Log.d(TAG ,"==========>Task Run In!");
            checkConnectState();
        }
    };

    class CheckModeForPauseTask extends TimerTask {
        @Override
        public void run()
        {
            if(mControlMode == DJIGroundStationTypeDef.GroundStationControlMode.GS_Mode_Pause_1 || mControlMode == DJIGroundStationTypeDef.GroundStationControlMode.GS_Mode_Pause_2 || mControlMode == DJIGroundStationTypeDef.GroundStationControlMode.GS_Mode_Gps_Atti){
                if(checkGetHomePoint()){
                    DJIDrone.getDjiGroundStation().pauseGroundStationTask(new DJIGroundStationHoverCallBack(){

                        @Override
                        public void onResult(DJIGroundStationTypeDef.GroundStationHoverResult result) {
                            // TODO Auto-generated method stub
                            String ResultsString = "pause return code =" + result.value();
                            handler.sendMessage(handler.obtainMessage(SHOWTOAST, ResultsString));

                            if(result == DJIGroundStationTypeDef.GroundStationHoverResult.GS_Hover_Successed){
                                if(mCheckModeForPauseTimer != null){
                                    mCheckModeForPauseTimer.cancel();
                                    mCheckModeForPauseTimer.purge();
                                    mCheckModeForPauseTimer = null;
                                }
                            }
                        }
                    });
                }
                return;
            }
            Log.d(TAG ,"mControlMode==========>"+mControlMode);
        }
    };

    //add by lizi end

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case SHOWTOAST:
                    setResultToToast((String) msg.obj);
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        show = AnimationUtils.loadAnimation(MainPage.this,R.anim.control_show);
        dis = AnimationUtils.loadAnimation(MainPage.this,R.anim.control_dis);

        initPopupWindow();

        SaveBitmap.createSDCardDir();

        fragmentManager = getFragmentManager();
        control = new Control();
        loadFragment = new LoadFragment();

        djiGLSurfaceView = (XKZGLSurfaceView) findViewById(R.id.DjiSurfaceView);
        //图像相关：

        start_control = (ImageButton) findViewById(R.id.start_control);
        koda = (ImageButton) findViewById(R.id.koda);
        alberm = (ImageButton) findViewById(R.id.alberm);
        mode_switcher = (ToggleButton) findViewById(R.id.mode_switcher);
        control_toucher = (RelativeLayout)findViewById(R.id.control_toucher);

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.loadfragment_container,loadFragment).commit();

        Thread loadThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true){
                    if (djiGLSurfaceView.dataArrived){
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.remove(loadFragment).commit();
                        break;
                    }
                }
            }
        });

        final RelativeLayout control_container = (RelativeLayout)findViewById(R.id.control_container);
        //进入遥控杆模式；
        start_control.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //add by Lizi
                InitJoyStick();
                //add by Lizi end

                fragmentTransaction = fragmentManager.beginTransaction();
                control_container.startAnimation(show);
                fragmentTransaction.add(R.id.control_container, control).commit();
                isControlShowed = true;
                Mode_Change(1);
            }
        });

        //触控离开摇杆模式;
        control_toucher.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isControlShowed) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            break;
                        case MotionEvent.ACTION_UP:
                            if (isControlShowed) {
                                fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.remove(control).commit();
                                isControlShowed = false;
                                Mode_Change(0);
                            }
                            break;
                    }
                    return true;
                }else {
                    return false;
                }
            }
        });

        alberm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainPage.this, Alberm.class);
                startActivity(intent);
            }
        });

        //快门和拍摄按钮；
        koda.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Mode == true) {   //拍照模式；
                    Bitmap bmp = djiGLSurfaceView.getSnapshot();
                    if (bmp != null) {
                        try {
                            SaveBitmap.save(bmp);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }

                    DJIDrone.getDjiCamera().startTakePhoto(new DJIExecuteResultCallback() {

                        @Override
                        public void onResult(DJIError mErr) {
                            // TODO Auto-generated method stub

                            Log.d("Main page", "Start Takephoto errorCode = " + mErr.errorCode);
                            Log.d("Main page", "Start Takephoto errorDescription = " + mErr.errorDescription);



                            String result = (mErr.errorCode == 0 ?
                                    "已保存至相册" : "拍照错误：" + DJIError.getErrorDescriptionByErrcode(mErr.errorCode));
                            handler.sendMessage(handler.obtainMessage(SHOWTOAST, result));
                        }
                    });
                } else { //摄像模式；
                    if (!isRecording) { //start recording
                        isRecording = true;
                        Mode_Change(2);

                        DJIDrone.getDjiCamera().startRecord(new DJIExecuteResultCallback() {

                            @Override
                            public void onResult(DJIError mErr) {
                                // TODO Auto-generated method stub
                                Log.d("Main page", "Start Recording errorCode = " + mErr.errorCode);
                                Log.d("Main page", "Start Recording errorDescription = " + mErr.errorDescription);

                                if (mErr.errorCode == 0) {// start recording without error
                                    //拍摄红灯亮
                                    isRecording = true;
                                } else {// meet error when starting recording
                                    String result = "拍摄错误：" + DJIError.getErrorDescriptionByErrcode(mErr.errorCode);
                                    handler.sendMessage(handler.obtainMessage(SHOWTOAST, result));
                                }
                            }

                        });
                    } else { //stop recording
                        DJIDrone.getDjiCamera().stopRecord(new DJIExecuteResultCallback() {

                            @Override
                            public void onResult(DJIError mErr) {
                                // TODO Auto-generated method stub
                                Log.d("Main page", "Stop Recording errorCode = " + mErr.errorCode);
                                Log.d("Main page", "Stop Recording errorDescription = " + mErr.errorDescription);

                                String result;
                                if (mErr.errorCode == 0) {// record successfully
                                    //拍摄红灯灭
                                    isRecording = false;
                                    result = "已保存至相册";

                                } else {
                                    result = "拍摄错误：" + DJIError.getErrorDescriptionByErrcode(mErr.errorCode);
                                }
                                handler.sendMessage(handler.obtainMessage(SHOWTOAST, result));
                            }
                        });

                        Mode_Change(0);
                        isRecording = false;
                    }
                }
            }
        });

        mode_switcher.setChecked(true);     //拍照、摄像模式切换；
        mode_switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mode_switcher.setChecked(isChecked);
                if (isChecked == true) {
                    Mode = true;
                    koda.setImageResource(R.drawable.koda);
                } else {
                    Mode = false;
                    koda.setImageResource(R.drawable.video_koda);
                }
            }
        });


        textView = (TextView)findViewById(R.id.textView1);
        djiGLSurfaceView.setOnTouchListener(new View.OnTouchListener() {

            int mode = 0;

            int mode_NONE = 0;
            int mode_MOVE = 1;
            int mode_ZOOM = 2;

            int pointCount = 0;

            float startX0;
            float startY0;
            float X0;
            float Y0;
            float changedX0;    //正值镜头向左，负值镜头向右；
            float changedY0;    //正值镜头向上，负值镜头向下；

            float startX1;
            float startY1;
            float X1;
            float Y1;
            float startX2;
            float startY2;
            float X2;
            float Y2;
            float OLDdiatance = 0;
            float NEWdistance = 0;
            float changedDistance = 0;  //大于0放大,小于0缩小；

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                pointCount = event.getPointerCount();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        mode = mode_MOVE;
                        startX0 = event.getX();
                        startY0 = event.getY();
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        if (event.getPointerCount() == 2) {
                            mode = mode_ZOOM;
                            startX1 = event.getX(0);
                            startY1 = event.getY(0);
                            startX2 = event.getX(1);
                            startY2 = event.getY(1);
                            OLDdiatance = Distance(startX1,startY1,startX2,startY2);
                        }else if (event.getPointerCount() > 2){
                            mode = mode_NONE;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == mode_MOVE){
                            X0 = event.getX();
                            Y0 = event.getY();
                            changedX0 = X0 - startX0;
                            changedY0 = Y0 - startY0;
                            flightControl(changedX0,changedY0); //移动;
                            //startX0 = X0;
                            //startY0 = Y0;
                        }else if (mode == mode_ZOOM){
                            X1 = event.getX(0);
                            Y1 = event.getY(0);
                            X2 = event.getX(1);
                            Y2 = event.getY(1);
                            NEWdistance = Distance(X1,Y1,X2,Y2);
                            changedDistance = NEWdistance - OLDdiatance;
                            if (changedDistance > 100){
                                //放大；
                                handzoom(changedDistance);
                               // OLDdiatance = NEWdistance;
                            }else if (changedDistance < -100){
                                //缩小；
                                handzoom(changedDistance);
                               // OLDdiatance = NEWdistance;
                            }
                        }else {
                            Reset();
                            return true;
                        }
                        textView.setText(changedX0 + "  " + changedY0 + "\n" + OLDdiatance + "  " + NEWdistance + "  " + changedDistance);
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        if (event.getPointerCount() < 2) {
                            mode = mode_MOVE;
                        }else {
                            mode = mode_NONE;
                            Reset();
                        }
                        textView.setText(changedX0 + "  " + changedY0 + "\n" + OLDdiatance + "  " + NEWdistance + "  " + changedDistance);
                        break;
                    case MotionEvent.ACTION_UP:
                        mode = mode_NONE;
                        stopControl();
                        Reset();
                        textView.setText(changedX0 + "  " + changedY0 + "\n" + OLDdiatance + "  " + NEWdistance + "  " + changedDistance);
                        break;
                }
                return true;
            }

            private float Distance(float x1,float y1,float x2,float y2){
                double d = Math.sqrt(Math.pow((x1 - x2),2) + Math.pow((y1 - y2),2));
                return (float)d;
            }

            private void Reset(){
                startX0 = 0;
                startY0 = 0;
                X0 = 0;
                Y0 = 0;
                changedX0 = 0;
                changedY0 = 0;
                startX1 = 0;
                startY1 = 0;
                X1 = 0;
                Y1 = 0;
                startX2 = 0;
                startY2 = 0;
                X2 = 0;
                Y2 = 0;
                OLDdiatance = 0;
                NEWdistance = 0;
                changedDistance = 0;
            }
        });

        setDjiDrone();

        loadThread.start();


        bTimer = new Timer();
        Task task = new Task();
        bTimer.schedule(task, 0, 500);
        DJIDrone.getDjiBattery().startUpdateTimer(2000);
        // BateryInfo

    }

    private void Mode_Change(int type) {
        if (type == 2){
            start_control.setVisibility(View.INVISIBLE);
            alberm.setVisibility(View.INVISIBLE);
            mode_switcher.setVisibility(View.INVISIBLE);
        }else if (type == 1) {
            start_control.setVisibility(View.INVISIBLE);
            koda.setVisibility(View.INVISIBLE);
            alberm.setVisibility(View.INVISIBLE);
            mode_switcher.setVisibility(View.INVISIBLE);
        } else {
            start_control.setVisibility(View.VISIBLE);
            koda.setVisibility(View.VISIBLE);
            alberm.setVisibility(View.VISIBLE);
            mode_switcher.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        menu.add("menu");
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (pw != null) {
            if (!pw.isShowing()) {
                //弹出显示   在指定的位置(parent)  最后两个参数 是相对于 x / y 轴的坐标
                pw.showAtLocation(findViewById(R.id.tv), Gravity.CENTER, 0, 300);
            }
        }
        return false;// 返回为true 则显示系统menu
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                if (djiGLSurfaceView != null) {
                    if (djiGLSurfaceView.isInFaceDetection()) {
                        djiGLSurfaceView.exitFaceDetection();
                    } else {
                        djiGLSurfaceView.startFaceDetection();

                    }
                }
                pw.dismiss(); //menu消失；
                break;
            case 1:
                Toast.makeText(this, "第二个", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(this, "第三个", Toast.LENGTH_LONG).show();
                break;
            case 3:
                Toast.makeText(this, "第四个", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void initPopupWindow() {
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.menu, null);
        GridView grid1 = (GridView) view.findViewById(R.id.menuGridChange);
        grid1.setOnItemClickListener(this);
        grid1.setFocusableInTouchMode(true);
        grid1.setAdapter(new ImageAdapter(this));
        //用Popupwindow弹出menu
        pw = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pw.setFocusable(true);
        pw.setBackgroundDrawable(new ColorDrawable());
        grid1.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_MENU) && (pw.isShowing())) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {

        private Context context;

        public ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return resArray.length;
        }

        @Override
        public Object getItem(int arg0) {
            return resArray[arg0];
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            LinearLayout linear = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            linear.setOrientation(LinearLayout.VERTICAL);
            //装进一张图片
            ImageView iv = new ImageView(context);
            iv.setImageBitmap(((BitmapDrawable) context.getResources().getDrawable(resArray[arg0])).getBitmap());
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params2.gravity = Gravity.CENTER;
            linear.addView(iv, params2);
            //显示文字
            TextView tv = new TextView(context);
            tv.setText(title[arg0]);
            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params3.gravity = Gravity.CENTER;

            linear.addView(tv, params3);

            return linear;
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG,"mainpage on pause");

        if(mTimer!=null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        DJIDrone.getDjiMC().stopUpdateTimer();
        //DJIDrone.getDjiGroundStation().stopUpdateTimer();
        //DJIDrone.getDjiBattery().stopUpdateTimer();
        super.onPause();
    }

    @Override
    protected void onResume() {
        //Log.i(TAG,"mainpage on resume");
        // TODO Auto-generated method stub

        super.onResume();
    }


    @Override
    protected void onDestroy() {
        //Log.i(TAG,"mainpage on destroy");
        super.onDestroy();
        onUnInitSDK();
    }

    private BaseLoaderCallback opencvLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                Log.i(TAG, "OpenCV loaded successfully!");
            } else {
                Log.e(TAG, "fail to load OpenCV");
                super.onManagerConnected(status);
            }
        }
    };

    /*
        dji drone configuration by JGUO
     */
    private void setDjiDrone() {
        new Thread() {
            public void run() {
                try {
                    DJIDrone.checkPermission(getApplicationContext(), new DJIGerneralListener() {
                        @Override
                        public void onGetPermissionResult(int result) {
                            if (result == 0) {
                                handler.sendMessage(handler.obtainMessage(SHOWTOAST, "permission got! Connect Drone WiFi and restart"));
                                //Toast.makeText(MainPage.this,"permission got! Connect Drone WiFi and restart",Toast.LENGTH_SHORT).show();
                                setDjiGLSurfaceView();

                            } else {
                                handler.sendMessage(handler.obtainMessage(SHOWTOAST, "permission denied, error code:" + result));
                                //Toast.makeText(MainPage.this,"permission denied, error code:" + result, Toast.LENGTH_SHORT).show();
                                Log.e("main activity", "onGetPermissionResult = " + result);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("main activity", "check permission failed!");
                }
            }
        }.start();
        onInitSDK();
    }
    /*
        dji sdk initialization and disconnection by JCGUO
     */
    private void onInitSDK() {
        DJIDrone.initWithType(DJIDroneTypeDef.DJIDroneType.DJIDrone_Vision);
        DJIDrone.connectToDrone();
    }

    private void onUnInitSDK() {
        DJIDrone.disconnectToDrone();
        djiGLSurfaceView.destroy();
        DJIDrone.getDjiCamera().setReceivedVideoDataCallBack(null);
        if(mCheckModeForPauseTimer != null){
            mCheckModeForPauseTimer.cancel();
            mCheckModeForPauseTimer.purge();
            mCheckModeForPauseTimer = null;
        }
    }

    /*
        connection state check by JCGUO
     */
    private void checkConnectState() {
        MainPage.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean connectState = DJIDrone.getDjiCamera().getCameraConnectIsOk();

                if (connectState) {
                    //show connection success prompt    -> David
                } else {
                    //show conneciton failure prompt    -> David
                }
            }
        });
    }

    /*
        dji GL surface view configuration by JCGUO
     */
    private void setDjiGLSurfaceView() {
        djiGLSurfaceView.setStreamType(DJICameraSettingsTypeDef.CameraPreviewResolustionType.Resolution_Type_640x480_30fps);
        djiGLSurfaceView.listener = MainPage.this;
        djiGLSurfaceView.start();


        receivedVideoDataCallBack = new DJIReceivedVideoDataCallBack() {
            @Override
            public void onResult(byte[] videoBuffer, int size) {
                //
                djiGLSurfaceView.setDataToDecoder(videoBuffer, size);
                //Log.i(TAG,"send data to buffer, size " + size);
            }
        };

        DJIDrone.getDjiCamera().setReceivedVideoDataCallBack(receivedVideoDataCallBack);
/*
        mBattryUpdateInfoCallBack = new DJIBattryUpdateInfoCallBack(){
            @Override
            public void onResult(DJIBatteryProperty state) {
                // TODO Auto-generated method stub
                power = (int)100*state.currentElectricity/state.fullChargeVolume;
                Log.e(TAG, "Power = " + power);
                handler.sendMessage(handler.obtainMessage(SHOWTOAST, "Power = " + power));
            }
        };
        DJIDrone.getDjiBattery().setBattryUpdateInfoCallBack(mBattryUpdateInfoCallBack);
*/
    }

    /*
        show toast with handler
     */
    private void setResultToToast(String result) {
        Toast.makeText(MainPage.this, result, Toast.LENGTH_SHORT).show();
    }

    /*
         Init GroundStation  add by Lizi
     */
    private void InitJoyStick() {

        mTimer = new Timer();
        Task task = new Task();
        mTimer.schedule(task, 0, 500);

        DJIDrone.getDjiMC().startUpdateTimer(1000);
        DJIDrone.getDjiGroundStation().startUpdateTimer(1000);



        mMcuUpdateStateCallBack=new DJIMcuUpdateStateCallBack() {
            @Override
            public void onResult (DJIMainControllerSystemState state){
                // TODO Auto-generated method stub
                //Log.e(TAG, "DJIMainControllerSystemState homeLocationLatitude " +state.homeLocationLatitude);
                //Log.e(TAG, "DJIMainControllerSystemState homeLocationLongitude " +state.homeLocationLongitude);
                //handler.sendMessage(handler.obtainMessage(SHOWTOAST, "Pitch " + Pitch_now + "Roll" + Roll_now));
                sb = new StringBuffer();
                Pitch_now = state.pitch;    //获取飞机姿态
                Roll_now = state.roll;
                Log.i(TAG, "Pitch " + Pitch_now + "Roll" + Roll_now);
                homeLocationLatitude = state.homeLocationLatitude;
                homeLocationLongitude = state.homeLocationLongitude;

                aircraftLocationLatitude = state.phantomLocationLatitude;
                aircraftLocationLongitude = state.phantomLocationLongitude;

                isFlying = state.isFlying;

                if (homeLocationLatitude != -1 && homeLocationLongitude != -1 && homeLocationLatitude != 0 && homeLocationLongitude != 0) {
                    getHomePiontFlag = true;
                    //onPause();
                } else {
                    getHomePiontFlag = false;
                }
            }
        };

        DJIDrone.getDjiMC().setMcuUpdateStateCallBack(mMcuUpdateStateCallBack);
        mGroundStationFlyingInfoCallBack=new DJIGroundStationFlyingInfoCallBack() {
            @Override
            public void onResult (DJIGroundStationFlyingInfo flyingInfo){
                // TODO Auto-generated method stub
                //Log.e(TAG, "DJIGroundStationFlyingInfo homeLocationLatitude " +flyingInfo.homeLocationLatitude);
                //Log.e(TAG, "DJIGroundStationFlyingInfo homeLocationLongitude " +flyingInfo.homeLocationLongitude);
                mControlMode = flyingInfo.controlMode;
            }
        };
        DJIDrone.getDjiGroundStation().

        setGroundStationFlyingInfoCallBack(mGroundStationFlyingInfoCallBack);
        mTask = new DJIGroundStationTask();
    }
    /*
         JoyStick control add by lizi
    */
    private boolean checkGetHomePoint(){
        if(!getHomePiontFlag){
            setResultToToast(getString(R.string.gs_not_get_home_point));
        }
        return getHomePiontFlag;
    }
    /*
        OneKeyfly  add by lizi
    */
    public void oneKeyfly(){
        /*
            delete after testing
         */
        try{
            Toast.makeText(MainPage.this, "one key fly", Toast.LENGTH_SHORT).show();
        }catch(Exception e) {
            e.printStackTrace();
        }

        if(!checkGetHomePoint()) {
            return;
        }

        if(!isFlying){
            DJIDrone.getDjiGroundStation().openGroundStation(new DJIGroundStationExecutCallBack(){

                @Override
                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                    // TODO Auto-generated method stub
                    String ResultsString = "opengs return code =" + result.value();
                    handler.sendMessage(handler.obtainMessage(SHOWTOAST, ResultsString));

                    if(result == DJIGroundStationTypeDef.GroundStationResult.GS_Result_Successed){

                        //one key fly
                        DJIDrone.getDjiGroundStation().onKeyFly(new DJIGroundStationOneKeyFlyCallBack(){
                            @Override
                            public void onResult(DJIGroundStationTypeDef.GroundStationOneKeyFlyResult result) {
                                // TODO Auto-generated method stub

                                String ResultsString = "one key fly return code =" + result.value();
                                handler.sendMessage(handler.obtainMessage(SHOWTOAST, ResultsString));

                                if(result == DJIGroundStationTypeDef.GroundStationOneKeyFlyResult.GS_One_Key_Fly_Successed){
                                    mCheckModeForPauseTimer = new Timer();
                                    CheckModeForPauseTask mCheckTask = new CheckModeForPauseTask();
                                    mCheckModeForPauseTimer.schedule(mCheckTask, 100, 1000);
                                }
                            }

                        });
                    }
                }

            });
        }
        else{
            DJIDrone.getDjiGroundStation().openGroundStation(new DJIGroundStationExecutCallBack(){

                @Override
                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                    // TODO Auto-generated method stub
                    String ResultsString = "opengs return code =" + result.value();
                    handler.sendMessage(handler.obtainMessage(SHOWTOAST, ResultsString));

                    if(result == DJIGroundStationTypeDef.GroundStationResult.GS_Result_Successed){

                        mCheckModeForPauseTimer = new Timer();
                        CheckModeForPauseTask mCheckTask = new CheckModeForPauseTask();
                        mCheckModeForPauseTimer.schedule(mCheckTask, 100, 1000);
                    }
                    else{
                       // DealErrorWithVibrate();
                    }
                }

            });
        }

    }

    /*
      模拟变焦，传入参数为变焦条返回值
    */
    private void zoom(final int foucus_before, final int focus_now){
        zoomflag=true;
        Log.i("start", "flag");
        zoomThread = new Thread(){
            public void run() {
                startTime = System.currentTimeMillis();   //获取开始时间
                if (focus_now - foucus_before > 0) {   //斜下降
                    int v=0;
                    if(focus_now<=0)  v=100;
                    else if(focus_now>0) v = 300;
                    while (zoomflag) {
                        zoom_cnt++;
                        if (zoom_cnt == 4) {   //下降
                            zoom_cnt = 0;
                            Log.e("down", zoom_cnt + "");
                            DJIDrone.getDjiGroundStation().setAircraftJoystick(0, v, 0, 2, new DJIGroundStationExecutCallBack() {
                                @Override
                                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                    // TODO Auto-generated method stub
                                }

                            });
                            try {
                                Thread.currentThread().sleep(50);  //增大延迟时间可加速降落
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } else {     //悬停，降低下降速率
                           // Log.i("zoom cnt2", zoom_cnt + "");
                            DJIDrone.getDjiGroundStation().setAircraftJoystick(0, v, 0, 0, new DJIGroundStationExecutCallBack() {
                                @Override
                                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                    // TODO Auto-generated method stub
                                }

                            });
                            long endTime = System.currentTimeMillis(); //获取结束时间
                            if (endTime - startTime >=(focus_now - foucus_before)*50 ) {    //copter down end after 10s
                                new Thread() {
                                    public void run() {    //悬停
                                        DJIDrone.getDjiGroundStation().setAircraftJoystick(0,0, 0, 0,new DJIGroundStationExecutCallBack() {

                                            @Override
                                            public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                                // TODO Auto-generated method stub

                                            }

                                        });
                                    }
                                }.start();
                                zoomflag = false;
                                break;
                            }
                        }
                    }
                }
                else  if (focus_now - foucus_before < 0) {   //斜上升
                    while (zoomflag) {
                        zoom_cnt++;
                        if (zoom_cnt == 4) {   //上升
                            zoom_cnt = 0;
                            Log.e("up", zoom_cnt + "");
                            DJIDrone.getDjiGroundStation().setAircraftJoystick(0, -300, 0, 1, new DJIGroundStationExecutCallBack() {
                                @Override
                                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                    // TODO Auto-generated method stub
                                }

                            });
                            try {
                                Thread.currentThread().sleep(100);   //增大延迟时间可加速上升
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } else {     //悬停，降低上升速率
                            //Log.i("zoom cnt2", zoom_cnt + "");
                            DJIDrone.getDjiGroundStation().setAircraftJoystick(0, -300, 0, 0, new DJIGroundStationExecutCallBack() {
                                @Override
                                public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                    // TODO Auto-generated method stub
                                }

                            });
                            long endTime = System.currentTimeMillis(); //获取结束时间
                            if (endTime - startTime >=(foucus_before - focus_now)*50 ) {    //copter down end after 10s
                                new Thread() {
                                    public void run() {    //悬停
                                        DJIDrone.getDjiGroundStation().setAircraftJoystick(0, 0, 0, 0,new DJIGroundStationExecutCallBack() {

                                            @Override
                                            public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                                // TODO Auto-generated method stub

                                            }

                                        });
                                    }
                                }.start();
                                zoomflag = false;
                                break;
                            }
                        }
                    }
                }
            }
        };
        zoomThread.start();
    }

      /*
       zoom control UAV by lizi
     */

    private void handzoom(float V){
        int responseRatio = 3;   //响应速度

        if(V<0) {  //上升缩小
            V=V+100;
            zoomup_cnt=0;
            throttle = 1;
        }
        else if(V >0){  //下降放大
            V=V-100;
            throttle = 2;
            zoomdown_cnt=0;
        }
        else {
            zoomup_cnt++;
            zoomdown_cnt++;
            throttle = 0;
        }
        if(V<0) pitch = (int) (responseRatio * (V)/1.4);
        else if(V>=0) pitch = (int) (responseRatio * (V)/2.8);
        if (pitch > 800) pitch = 800;
        if (pitch < -800) pitch = -800;


        Thread handzoomThread = new Thread(){
            public void run(){
                DJIDrone.getDjiGroundStation().setAircraftJoystick(0,pitch, roll, throttle,new DJIGroundStationExecutCallBack(){
                    @Override
                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        };
        handzoomThread.start();
    }
    /*
        main control UAV by lizi
     */

    private void flightControl(float Vx,float Vy){
        int responseRatio = 3;   //响应速度

        float Threhold=300;

         /* if(Vx>-Threhold&&Vx<Threhold) roll =0;
          else if(Vx>Threhold) {
              roll = (int) (-responseRatio * (Vx - Threhold));
          }
          else if(Vx<-Threhold) {
              roll = (int) (-responseRatio * (Vx + Threhold));
          }*/
          roll = (int) (-responseRatio * Vx);
          if (roll > 1000) roll = 1000;
          if (roll < -1000) roll = -1000;


          /*if(Vy>-Threhold&&Vy<Threhold) pitch =0;
          else  if(Vy>Threhold) {
              pitch = (int) (responseRatio * (Vy - Threhold)/1.4);
          }
          else if(Vy<-Threhold) {
              pitch = (int) (responseRatio * (Vy + Threhold)/1.4);
          }*/
            if(Vy>50&&controlup_cnt == 1) {  //上升
                controlup_cnt=0;
                 throttle = 1;
            }
            else if(Vy < -100&&controldown_cnt==2){  //下降
               throttle = 2;
               controldown_cnt=0;
            }
            else {
                controlup_cnt++;
                controldown_cnt++;
                throttle = 0;
            }
            if(Vy<0) pitch = (int) (responseRatio * (Vy)/1.4);
            else if(Vy>=0) pitch = (int) (responseRatio * (Vy)/2.8);
            if (pitch > 800) pitch = 800;
            if (pitch < -800) pitch = -800;


         Thread mainControlThread = new Thread(){
             public void run(){
                DJIDrone.getDjiGroundStation().setAircraftJoystick(0,pitch, roll, throttle,new DJIGroundStationExecutCallBack(){
                    @Override
                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                        // TODO Auto-generated method stub
                    }
                });
             }
         };
        mainControlThread.start();
    }
    /*
    follow control UAV by lizi
 */
    public void followControl(int x,int y,int x0, int y0){
        int responseRatio = 3;   //响应速度

        //float Threhold=2;
        roll = -responseRatio*(x - x0);

        if (roll > 1000) roll = 1000;
        if (roll < -1000) roll = -1000;


        if (pitch > 1000) pitch = 1000;
        if (pitch < -1000) pitch = -1000;


        Thread flowControlThread = new Thread(){
            public void run(){
                DJIDrone.getDjiGroundStation().setAircraftJoystick(0,0, roll, 0,new DJIGroundStationExecutCallBack(){
                    @Override
                    public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        };
        flowControlThread.start();
    }

    public void stopControl(){
        final int stopRatio = 80;       //收敛速度 80
        final int sleeptime = 500;       //1000
         stopflag=true;
         final Thread stopControlThread = new Thread(){
            public void run()
            {
                while (stopflag) {
                        Log.e("stop",  "stop!!");
                        DJIDrone.getDjiGroundStation().setAircraftJoystick(0, -stopRatio*((int)180-(int)Pitch_now),stopRatio*((int)Roll_now-180), 0, new DJIGroundStationExecutCallBack() {
                            @Override
                            public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                                // TODO Auto-generated method stub
                            }

                        });
                        try {
                            Thread.currentThread().sleep(sleeptime);
                            
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    DJIDrone.getDjiGroundStation().setAircraftJoystick(0, 0, 0, 0, new DJIGroundStationExecutCallBack() {
                        @Override
                        public void onResult(DJIGroundStationTypeDef.GroundStationResult result) {
                            // TODO Auto-generated method stub
                        }

                     });
                    stopflag = false;
                }
            }
        };
        stopControlThread.start();
    }

    //add by lizi end
}

