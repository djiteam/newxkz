package com.example.firmament.dji;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

import org.opencv.core.Rect;

import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

import dji.sdk.api.Camera.DJICameraSettingsTypeDef;
import dji.sdk.natives.CamShow;

/**
 * Created by jcguo on 14/12/1.
 */
public class XKZGLSurfaceView extends GLSurfaceView {
    private static String TAG = "MyGLSurfaceView.java";
    private static final boolean DEBUG = false;
    private static int w = 0;
    private static int h = 0;

    private int initialTime = 0;

    private boolean isPause = false;
    private boolean requestSnapshot = false;

    private Bitmap droneBitmap;
    private XKZFaceDetector detector;
    private int detectTimer = 0;
    private boolean faceDetectionMode = false;

    public boolean dataArrived = false;

    public FaceDetectionListener listener;

    private boolean first = true;

    private int x0 = 630,y0 = 280;

    public XKZGLSurfaceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(false, 0, 0);
        detector = new XKZFaceDetector(context);
    }

    public XKZGLSurfaceView(Context context)
    {
        super(context);
        init(false, 0, 0);
        detector = new XKZFaceDetector(context);
    }

    public XKZGLSurfaceView(Context context, boolean translucent, int depth, int stencil)
    {
        super(context);
        init(translucent, depth, stencil);
        detector = new XKZFaceDetector(context);
    }

    private void init(boolean translucent, int depth, int stencil)
    {
        if (translucent) {
            getHolder().setFormat(-3);
        }

        setEGLContextFactory(new ContextFactory());

        setEGLConfigChooser(translucent ?
                new ConfigChooser(8, 8, 8, 8, depth, stencil) :
                new ConfigChooser(5, 6, 5, 0, depth, stencil));

        setRenderer(new MyRenderer());
        setRenderMode(0);


    }

    private static void checkEglError(String prompt, EGL10 egl)
    {
        int error;
        while ((error = egl.eglGetError()) != 12288)
        {
            Log.e(TAG, String.format("%s: EGL error: 0x%x", new Object[]{prompt, Integer.valueOf(error)}));
        }
    }

    public int fcb()
    {
        requestRender();
        return 0;
    }

    private int getType(int type)
    {
        int result = 1;
        if (type == 0) {
            result = 1;
        }
        else if (type == 1) {
            result = 2;
        }
        else if (type == 2) {
            result = 4;
        }
        else if (type == 3) {
            result = 8;
        }
        return result;
    }

    public boolean start()
    {
        new Thread() {
            public void run() {
                CamShow.native_setOnStreamCB(XKZGLSurfaceView.this, "fcb");
            }
        }.start();
        return true;
    }

    public boolean setStreamType(final DJICameraSettingsTypeDef.CameraPreviewResolustionType type)
    {
        new Thread() {
            public void run() {
                CamShow.native_pauseStream(true);
                CamShow.native_setType(XKZGLSurfaceView.this.getType(type.value()));
                CamShow.native_pauseStream(false);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        return true;
    }

    public boolean setDataToDecoder(byte[] videoBuffer, int size) {
        boolean result = false;
        int returnVal = -1;
        returnVal = CamShow.native_setDataToDecoder(videoBuffer, size);
        if (returnVal == 0) {
            result = true;
        }
        return result;
    }

    public boolean pause()
    {
        this.isPause = true;
        CamShow.native_pauseStream(true);
        return true;
    }

    public boolean resume()
    {
        CamShow.native_pauseStream(false);
        this.isPause = false;
        return true;
    }

    public boolean destroy()
    {
        this.isPause = false;
        this.dataArrived = false;
        return true;
    }

    public boolean getIsPause()
    {
        return this.isPause;
    }

    private void setIsPause(boolean isPause) {
        this.isPause = isPause;
    }

    private static class ConfigChooser implements GLSurfaceView.EGLConfigChooser
    {
        private static int EGL_OPENGL_ES2_BIT = 4;
        private static int[] s_configAttribs2 = {
                12324, EGL_OPENGL_ES2_BIT,
                12323, EGL_OPENGL_ES2_BIT,
                12322, EGL_OPENGL_ES2_BIT,
                12352, EGL_OPENGL_ES2_BIT,
                12344 };
        protected int mRedSize;
        protected int mGreenSize;
        protected int mBlueSize;
        protected int mAlphaSize;
        protected int mDepthSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ConfigChooser(int r, int g, int b, int a, int depth, int stencil)
        {
            this.mRedSize = r;
            this.mGreenSize = g;
            this.mBlueSize = b;
            this.mAlphaSize = a;
            this.mDepthSize = depth;
            this.mStencilSize = stencil;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
        {
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, s_configAttribs2, null, 0, num_config);
            int numConfigs = num_config[0];
            if (numConfigs <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }

            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, s_configAttribs2, configs, numConfigs, num_config);

            return chooseConfig(egl, display, configs);
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            for (EGLConfig config : configs) {
                int d = findConfigAttrib(egl, display, config, 12325, 0);
                int s = findConfigAttrib(egl, display, config, 12326, 0);

                if ((d >= this.mDepthSize) && (s >= this.mStencilSize))
                {
                    int r = findConfigAttrib(egl, display, config, 12324, 0);
                    int g = findConfigAttrib(egl, display, config, 12323, 0);
                    int b = findConfigAttrib(egl, display, config, 12322, 0);
                    int a = findConfigAttrib(egl, display, config, 12321, 0);
                    if ((r == this.mRedSize) && (g == this.mGreenSize) && (b == this.mBlueSize) && (a == this.mAlphaSize)) return config;
                }
            }
            return null;
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue)
        {
            if (egl.eglGetConfigAttrib(display, config, attribute, this.mValue)) {
                return this.mValue[0];
            }
            return defaultValue;
        }

        private void printConfigs(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            int numConfigs = configs.length;
            Log.w(XKZGLSurfaceView.TAG, String.format("%d configurations", new Object[] { Integer.valueOf(numConfigs) }));
            for (int i = 0; i < numConfigs; i++) {
                Log.w(XKZGLSurfaceView.TAG, String.format("Configuration %d:\n", new Object[] { Integer.valueOf(i) }));
                printConfig(egl, display, configs[i]);
            }
        }

        private void printConfig(EGL10 egl, EGLDisplay display, EGLConfig config) {
            int[] attributes = {
                    12320,
                    12321,
                    12322,
                    12323,
                    12324,
                    12325,
                    12326,
                    12327,
                    12328,
                    12329,
                    12330,
                    12331,
                    12332,
                    12333,
                    12334,
                    12335,
                    12336,
                    12337,
                    12338,
                    12339,
                    12340,
                    12343,
                    12342,
                    12341,
                    12345,
                    12346,
                    12347,
                    12348,
                    12349,
                    12350,
                    12351,
                    12352,
                    12354 };

            String[] names = {
                    "EGL_BUFFER_SIZE",
                    "EGL_ALPHA_SIZE",
                    "EGL_BLUE_SIZE",
                    "EGL_GREEN_SIZE",
                    "EGL_RED_SIZE",
                    "EGL_DEPTH_SIZE",
                    "EGL_STENCIL_SIZE",
                    "EGL_CONFIG_CAVEAT",
                    "EGL_CONFIG_ID",
                    "EGL_LEVEL",
                    "EGL_MAX_PBUFFER_HEIGHT",
                    "EGL_MAX_PBUFFER_PIXELS",
                    "EGL_MAX_PBUFFER_WIDTH",
                    "EGL_NATIVE_RENDERABLE",
                    "EGL_NATIVE_VISUAL_ID",
                    "EGL_NATIVE_VISUAL_TYPE",
                    "EGL_PRESERVED_RESOURCES",
                    "EGL_SAMPLES",
                    "EGL_SAMPLE_BUFFERS",
                    "EGL_SURFACE_TYPE",
                    "EGL_TRANSPARENT_TYPE",
                    "EGL_TRANSPARENT_RED_VALUE",
                    "EGL_TRANSPARENT_GREEN_VALUE",
                    "EGL_TRANSPARENT_BLUE_VALUE",
                    "EGL_BIND_TO_TEXTURE_RGB",
                    "EGL_BIND_TO_TEXTURE_RGBA",
                    "EGL_MIN_SWAP_INTERVAL",
                    "EGL_MAX_SWAP_INTERVAL",
                    "EGL_LUMINANCE_SIZE",
                    "EGL_ALPHA_MASK_SIZE",
                    "EGL_COLOR_BUFFER_TYPE",
                    "EGL_RENDERABLE_TYPE",
                    "EGL_CONFORMANT"
            };
            int[] value = new int[1];
            for (int i = 0; i < attributes.length; i++) {
                int attribute = attributes[i];
                String name = names[i];
                if (egl.eglGetConfigAttrib(display, config, attribute, value))
                    Log.w(XKZGLSurfaceView.TAG, String.format("  %s: %d\n", new Object[] { name, Integer.valueOf(value[0]) }));
            }
        }
    }

    private static class ContextFactory implements GLSurfaceView.EGLContextFactory
    {
        private static int EGL_CONTEXT_CLIENT_VERSION = 12440;

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
            Log.w(XKZGLSurfaceView.TAG, "creating OpenGL ES 2.0 context");
            XKZGLSurfaceView.checkEglError("Before eglCreateContext", egl);
            int[] attrib_list = { EGL_CONTEXT_CLIENT_VERSION, 2, 12344 };
            EGLContext context = egl.eglCreateContext(display, eglConfig,
                    EGL10.EGL_NO_CONTEXT, attrib_list);
            XKZGLSurfaceView.checkEglError("After eglCreateContext", egl);
            return context;
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            egl.eglDestroyContext(display, context);
        }
    }

    private class MyRenderer implements GLSurfaceView.Renderer
    {

        private int time = 0;

        public MyRenderer()
        {
            super();
        }

        private Bitmap createBitmapFromGLSurfaceView(int x, int y, int w, int h, GL10 gl) throws OutOfMemoryError {
            int bitmapBuffer[] = new int[w * h];
            int bitmapSource[] = new int[w * h];
            IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
            intBuffer.position(0);

            try {
                gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
                for (int i = 0; i < h; i++) {
                    for (int j = 0; j < w; j++) {
                        int texturePixel = bitmapBuffer[i * w + j];
                        int blue    = (texturePixel >> 16) & 0xff;
                        int red     = (texturePixel << 16) & 0x00ff0000;
                        int pixel   = (texturePixel & 0xff00ff00) | red | blue;
                        bitmapSource[(h - i - 1) * w + j] = pixel;
                    }
                }
            } catch (GLException e) {
                return null;
            }

            return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
        }

        public void onDrawFrame(GL10 gl)
        {
            //Log.i(TAG,"draw a frame");

            if (!XKZGLSurfaceView.this.isPause) {
                CamShow.native_GLDrawFrame();

                if (!dataArrived) {
                    initialTime++;
                    Log.i(TAG,"time: " + initialTime);
                    if (initialTime > 5) {
                        dataArrived = true;
                    }
                }

                if (!faceDetectionMode) {
                    if (requestSnapshot) {
                        droneBitmap = createBitmapFromGLSurfaceView(0, 0, XKZGLSurfaceView.w, XKZGLSurfaceView.h, gl);
                        if (droneBitmap != null) {
                            Log.i(XKZGLSurfaceView.TAG, "Get snapshot(" + droneBitmap.getWidth() + " * "+ droneBitmap.getHeight() + ")" + time + " of GLSurfaceView successfully!");
                        } else {
                            Log.e(XKZGLSurfaceView.TAG, "Can not get snapshot of GLSurfaceView!");
                        }
                        requestSnapshot = false;
                    }
                } else {
                    detectTimer = (detectTimer + 1) % 5;
                    if (detectTimer == 0) {
                        Log.i(TAG,"now let's detect faces!");
                        droneBitmap = createBitmapFromGLSurfaceView(0, 0, XKZGLSurfaceView.w, XKZGLSurfaceView.h, gl);
                        if (droneBitmap != null) {
                            //Log.i(XKZGLSurfaceView.TAG, "Get snapshot(" + droneBitmap.getWidth() + " * "+ droneBitmap.getHeight() + ")" + time + " of GLSurfaceView successfully!");
                            Rect rect = detector.detectFaceRect(droneBitmap);
                            if (rect != null) {
                                Log.e(TAG,"face : (" + rect.x + "," + rect.y + "," + rect.width + "," + rect.height + ")");

                                if (first) {
                                    x0 = rect.x;
                                    y0 = rect.y;
                                    first = false;
                                } else {
                                    listener.followControl(rect.x, rect.y, x0, y0);
                                }
                            } else {
                                listener.stopControl();
                                first = true;
                            }
                        } else {
                            Log.e(XKZGLSurfaceView.TAG, "Can not get snapshot of GLSurfaceView!");
                        }
                    }
                }
            }
        }

        public void onSurfaceChanged(GL10 gl, int width, int height)
        {
            XKZGLSurfaceView.w = width;
            XKZGLSurfaceView.h = height;
            CamShow.native_GLInit(XKZGLSurfaceView.w, XKZGLSurfaceView.h);
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config)
        {
        }
    }

    public Bitmap getSnapshot() {
        requestSnapshot = true;
        while(requestSnapshot);
        return droneBitmap;
    }

    public void startFaceDetection() {
        faceDetectionMode = true;
    }

    public void exitFaceDetection() {
        faceDetectionMode = false;
    }

    public boolean isInFaceDetection() {
        return faceDetectionMode;
    }

    public interface FaceDetectionListener {
        public void followControl(int x,int y,int x0, int y0);
        public void stopControl();
    }
}
